package scripts.xPestControl.User;

import java.awt.Color;
import java.awt.Font;
import java.util.ArrayList;
import java.util.Arrays;

import org.tribot.api.Timing;
import org.tribot.api.util.ABCUtil;
import org.tribot.api2007.Interfaces;
import org.tribot.api2007.types.RSArea;
import org.tribot.api2007.types.RSInterfaceChild;
import org.tribot.api2007.types.RSTile;

import scripts.xPestControl.Utils.Entity;

public class Constants {
	public static final ABCUtil ANTIBAN = new ABCUtil();
	public static final RSArea NOVICE_BOAT_AREA = new RSArea(new RSTile(2660, 2643, 0), new RSTile(2663, 2638, 0));
	public static final RSTile NOVICE_BOAT_BOARDING_POSITION = new RSTile(2657, 2639, 0);
	public static final RSTile VOID_KNIGHT_EXCHANGE_POSITION = new RSTile(2662, 2650, 0);
	public static final String[] MONSTER_NAMES = new String[] {"Spinner", "Shifter", "Defiler", "Splatter", "Torcher", "Ravager"};
	public static final int BOAT_INTERFACE = 407;
	public static final int SHOP_INTERFACE = 267;
	public static final int BOAT_INTERFACE_POINTS_CHILD = 14;
	public static final int MINIGAME_INTERFACE = 408;
	public static final int MINIGAME_INTERFACE_ACTIVITY_CHILD = 11;
	public static final int EXCHANGE_CONFIRM_BOX_CHILD = 146;
	public static final int EXCHANGE_CONFIRM_ITEM_NAME_CHILD = 149;
	public static final long START_TIME = Timing.currentTimeMillis();
	public static final Color TRANSP_BLACK_COLOR = new Color(0, 0, 0, 170);
	public static final Color TRANSP_GREEN_COLOR = new Color(99, 209, 62, 170);
	public static final Font TEXT_FONT = new Font("Arial", 0, 13);
	public static final Font TEXT_ITALIC_FONT = new Font("Arial", 2, 11);
	public static final Font TITLE_FONT = new Font("Arial", 0, 18);

	public static String[] getMonsters()
	{
		ArrayList<String> monsters = new ArrayList(Arrays.asList(MONSTER_NAMES));
		if(true)
		{
			monsters = new ArrayList<String>();
			monsters.add("Spinner");
		}
		RSInterfaceChild shield = null;
		
		if(Variables.currentGoal == null)
			return monsters.toArray(new String[monsters.size()]);
		
		switch(Variables.currentGoal)
		{
			case WEST_PORTAL:
				shield = Interfaces.get(408, 17);
				break;
			case EAST_PORTAL:
				shield = Interfaces.get(408, 19);
				break;
			case SOUTH_EAST_PORTAL:
				shield = Interfaces.get(408, 21);
				break;
			case SOUTH_WEST_PORTAL:
				shield = Interfaces.get(408, 23);
				break;
		}
		
		if(shield != null && shield.isHidden())
			monsters.add("Portal");
		
		return monsters.toArray(new String[monsters.size()]);
	}
}
