package scripts.xPestControl.User;

import java.util.ArrayList;

import org.tribot.api2007.Equipment;
import org.tribot.api2007.Equipment.SLOTS;
import org.tribot.api2007.types.RSItem;
import org.tribot.api2007.types.RSItemDefinition;
import org.tribot.api2007.types.RSNPC;
import org.tribot.api2007.types.RSTile;

import scripts.xPestControl.GUI;
import scripts.xPestControl.Utils.BoatType;
import scripts.xPestControl.Utils.Entity;
import scripts.xPestControl.Utils.Game;
import scripts.xPestControl.Utils.GameType;
import scripts.xPestControl.Utils.Item;
import scripts.xPestControl.Utils.Node;
import scripts.xPestControl.Utils.Weapon;

public class Variables {

	public static final int specialAttackCost = 50;
	public static ArrayList<Node> nodes = new ArrayList<Node>();
	public static boolean stop = false;
	public static GUI gui = new GUI();
	public static Game game;
	public static int gamesWon = 0;
	public static int gamesLost = 0;
	public static int totalPoints = 0;
	public static Entity currentGoal = null;
	public static RSNPC currentMonster = null;
	public static ArrayList<Item> itemsToBuy = new ArrayList<Item>();
	public static boolean useQuickPray = true;
	
	public static int getSpecialAttackCost()
	{
		for(final Weapon weapon : Weapon.values())
		{
			if(weapon.isEquipped())
				return weapon.getSpecialAttackValue();
		}
		return 120;
	}
}
