package scripts.xPestControl.Utils;

import org.tribot.api.General;
import org.tribot.api2007.Player;

import scripts.xPestControl.User.Variables;

public abstract class Node {
	
	/**
     * Is action valid?
     * @return true when valid, false otherwise.
     */
    public abstract boolean isValid();
    
    /**
     * Execute action.
     */
    public abstract void execute();
}
