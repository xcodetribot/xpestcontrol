package scripts.xPestControl.Utils;

import java.awt.Point;
import java.awt.Rectangle;

public enum Item {
	VOID_KNIGHT_MACE("Void Knight Mace", 93, 250, new Rectangle(478, 196, 9, 50)),
	VOID_KNIGHT_TOP("Void Knight Top", 94, 250, new Rectangle(478, 196, 9, 50)),
	VOID_KNIGHT_ROBES("Void Knight Robes", 95, 250, new Rectangle(478, 196, 9, 50)),
	VOID_KNIGHT_GLOVES("Void Knight Gloves", 96, 150, new Rectangle(478, 196, 9, 50)),
	VOID_KNIGHT_MAGE_HELM("Void Knight Mage Helm", 119, 200, new Rectangle(478, 196, 9, 50)),
	VOID_KNIGHT_RANGER_HELM("Void Knight Ranger Helm", 120, 200, new Rectangle(478, 196, 9, 50)),
	VOID_KNIGHT_MELEE_HELM("Void Knight Melee Helm", 121, 200, new Rectangle(478, 196, 9, 50)),
	VOID_KNIGHT_SEAL("Void Knight Seal", 122, 10, new Rectangle(478, 196, 9, 50));
	
	private String name;
	private int childInterface;
	private int cost;
	private Rectangle scrollBarRect;

	Item(String name, int childInterface, int cost, Rectangle scrollBarRect)
	{
		this.name = name;
		this.childInterface = childInterface;
		this.cost = cost;
		this.scrollBarRect = scrollBarRect;
	}
	
	@Override
	public String toString()
	{
		return name;
	}
	
	/**
	 * Get the price of an item.
	 * @return integer of the amount of Pest Points the item costs.
	 */
	public int getCost()
	{
		return cost;
	}
	
	/**
	 * Get the child interface id of the item within the exchange window at the void knight.
	 * @return integer of the child interface id.
	 */
	public int getChildInterface()
	{
		return childInterface;
	}
	
	/**
	 * Get the rectangle of where to click te scrollbar within the exchange window to see the specified item.
	 * @return Rectangle of where to click to see item.
	 */
	public Rectangle getScrollBarRect()
	{
		return scrollBarRect;
	}
}
