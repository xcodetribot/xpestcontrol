package scripts.xPestControl.Utils;

import org.tribot.api2007.Equipment;
import org.tribot.api2007.Equipment.SLOTS;
import org.tribot.api2007.types.RSItem;
import org.tribot.api2007.types.RSItemDefinition;

public enum Weapon {

	DRAGON_DAGGER("Dragon dagger", 25, false),
	DRAGON_SCIMITAR("Dragon scimitar", 55, false),
	DRAGON_LONGSWORD("Dragon longsword", 25, false),
	DRAGON_MACE("Dragon mace", 25, false),
	DRAGON_BATTLEAXE("Dragon battleaxe", 100, false),
	DRAGON_HALBERD("Dragon halberd", 30, true),
	DRAGON_SPEAR("Dragon spear", 25, true),
	DRAGON_2H_SWORD("Dragon 2h sword", 25, true),
	ARMADYL_GODSWORD("Armadyl godsword", 50, true),
	BANDOS_GODSWORD("Bandos godsword", 100, true),
	SARADOMIN_GODSWORD("Saradomin godsword", 50, true),
	ZAMORAK_GODSWORD("Zamorak godsword", 60, true),
	ABYSSAL_WHIP("Abyssal whip", 50, false),
	ABYSSAL_TENTACLE("Abyssal tentacle", 50, false),
	BARRELCHEST_ANCHOR("Barrelchest anchor", 50, true),
	GRANITE_MAUL("Granite maul", 50, true),
	RUNE_CLAWS("Rune claws", 25, true),
	EXCALIBUR("Excalibur", 100, false),
	DARKLIGHT("Darklight", 50, false),
	BONE_DAGGER("Bone dagger", 75, false),
	BRINE_SABRE("Brine sabre", 75, false),
	ANCIENT_MACE("Ancient mace", 100, false),
	SARADOMIN_SWORD("Saradomin sword", 100, true),
	ZAMORAKIAN_SPEAR("Zamorakian spear", 25, true),
	ZAMORAKIAN_HASTA("Zamorakian hasta", 25, true),
	SARADOMINS_BLESSED_SWORD("Saradomin's blessed sword", 65, true),
	ARMADYL_CROSSBOW("Armadyl crossbow", 40, false),
	DARK_BOW("Dark bow", 55, true),
	MAGIC_SHORTBOW("Magic shortbow", 55, true),
	MAGIC_LONGBOW("Magic longbow", 35, true),
	KNIFE("Knife", -1, false);
		
	private String weaponName;
	private int specialAttackValue; //-1 = No special
	private boolean twoHanded;
	
	Weapon(String weaponName, int specialAttack, boolean twoHanded)
	{
		this.weaponName = weaponName;
		this.specialAttackValue = specialAttack;
		this.twoHanded = twoHanded;
	}
	
	@Override
	public String toString()
	{
		return weaponName;
	}
	
	public boolean hasSpecialAttack()
	{
		return specialAttackValue != -1;
	}
	
	public int getSpecialAttackValue()
	{
		return specialAttackValue;
	}
	
	public boolean isEquipped()
	{
		RSItem equippedItem = Equipment.getItem(SLOTS.WEAPON);
		if(equippedItem != null)
		{
			RSItemDefinition definition = equippedItem.getDefinition();
			if(definition != null)
			{
				String weaponName = definition.getName();
				if(weaponName != null)
				{
					return weaponName.equals(this.weaponName);
				}
			}
		}
		return false;
	}
	
	public boolean isTwoHanded()
	{
		return twoHanded;
	}
	
	public static Weapon getWeaponByName(String name)
	{
		for(Weapon weapon : Weapon.values())
		{
			//Using contains()-function in case the weapon is poisoned (like (P++)).
			if(name.toLowerCase().contains(weapon.toString().toLowerCase()))
				return weapon;
		}
		return null;
	}
}
