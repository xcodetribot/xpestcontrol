package scripts.xPestControl.Utils;

import org.tribot.api2007.Interfaces;
import org.tribot.api2007.Player;

import scripts.xPestControl.User.Constants;

public class Game {

	private GameType gameType;
	private BoatType boatType;
	
	public Game(GameType gType, BoatType bType)
	{
		gameType = gType;
		boatType = bType;
	}
	
	/**
	 * Is the player in the minigame?
	 * @return true if player is in minigame, false otherwise.
	 */
	public boolean inGame()
	{
		return Interfaces.get(Constants.MINIGAME_INTERFACE) != null;
	}
	
	/**
	 * Is the player in the boat area?
	 * @return true when the player is in the boat area, false otherwise.
	 */
	public boolean inBoatArea()
	{
		return boatType.getBoatArea().contains(Player.getPosition());
	}
	
	/**
	 * Get the current game type.
	 * @return the current GameType.
	 */
	public GameType getGameType()
	{
		return gameType;
	}
	
	/**
	 * Get the current boat type.
	 * @return the current BoatType.
	 */
	public BoatType getBoatType()
	{
		return boatType;
	}
}
