package scripts.xPestControl.Utils;

import org.tribot.api2007.types.RSArea;
import org.tribot.api2007.types.RSTile;

public enum BoatType {
	NOVICE("Novice", new RSTile(2657, 2639, 0), new RSArea(new RSTile(2660, 2643, 0), new RSTile(2663, 2638, 0)), 2), 
	INTERMEDIATE("Intermediate", new RSTile(2644, 2644, 0), new RSArea(new RSTile(2638, 2647, 0), new RSTile(2641, 2642, 0)), 3), 
	VETERAN("Veteran", new RSTile(2638, 2653, 0), new RSArea(new RSTile(2660, 2643, 0), new RSTile(2663, 2638, 0)), 4);
	
	private RSTile boardingTile;
	private String name;
	private RSArea boatArea;
	private int points;
	
	BoatType(String boatName, RSTile boardingTile, RSArea area, int points)
	{
		this.boardingTile = boardingTile;
		this.name = boatName;
		this.boatArea = area;
		this.points = points;
	}

	@Override
	public String toString()
	{
		return name;
	}
	
	/**
	 * Get the RSTile of the boarding position.
	 * @return the position of the boarding tile.
	 */
	public RSTile getTile()
	{
		return boardingTile;
	}
	
	/**
	 * Get the RSArea of the specified boat area.
	 * @return the RSArea of the specified boat area.
	 */
	public RSArea getBoatArea()
	{
		return boatArea;
	}
	
	/**
	 * Get the amount of points you get when you win a game for the specified boat.
	 * @return integer of the amount of points you get when you win the game.
	 */
	public int getPoints()
	{
		return points;
	}
}
