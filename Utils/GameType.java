package scripts.xPestControl.Utils;

import org.tribot.api2007.Player;
import org.tribot.api2007.types.RSTile;

import scripts.xPestControl.User.Variables;

public enum GameType {
	ATTACK_PORTALS("Attack monsters & portals"),
	PROTECT_VOID_KNIGHT("Protect Void Knight");

private RSTile destination;
private String name;

GameType(String name)
{
	this.name = name;
}

	/**
	 * Get the next destination. Destination is dynamic for portals, but not for PROTECT_VOID_KNIGHT.
	 * @return	RSTile of the next destination.
	 */
	public RSTile getNextDestination()
	{
		switch(this)
		{
		case PROTECT_VOID_KNIGHT:
			destination = Entity.VOID_KNIGHT.getPosition();
			Variables.currentGoal = Entity.VOID_KNIGHT;
			break;
		case ATTACK_PORTALS:
			destination = Entity.getNextPortal().getPosition();
			break;
		}
		
		return destination;
	}
	
	/**
	 * Is the player at it's destination?
	 * @return true if the distance between the destination and the player is <= 15, false otherwise.
	 */
	public boolean isAtDestination()
	{
		if(destination == null)
			return true;
		
		return destination.distanceTo(Player.getPosition()) <= 15;
	}

	@Override
	public String toString()
	{
		return name;
	}

}
