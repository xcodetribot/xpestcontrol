package scripts.xPestControl.Utils;

import org.tribot.api.General;
import org.tribot.api2007.Interfaces;
import org.tribot.api2007.Player;
import org.tribot.api2007.types.RSInterfaceChild;
import org.tribot.api2007.types.RSTile;

import scripts.xPestControl.User.Variables;


public enum Entity {
	WEST_PORTAL("West Portal"), 
	SOUTH_WEST_PORTAL("South-West Portal"), 
	SOUTH_EAST_PORTAL("South-East Portal"), 
	EAST_PORTAL("East Portal"),
	VOID_KNIGHT("Void Knight"),
	WEST_GATE("West Gate"),
	SOUTH_GATE("South Gate"),
	EAST_GATE("East Gate");
	
	private String name;
	private RSTile position;

	/**
	 * Constructor of Entity-class.
	 * @param name	the name of the entity;
	 */
	Entity(String name)
	{
		this.name = name;
		this.position = null;
	}
	
	/**
	 * Set the positions of all the entities within Pest Control.
	 * @param spawnLocation	the position of Squire when you spawn at Pest Control minigame.
	 */
	public void setPosition(RSTile spawnLocation)
	{
		switch(this)
		{
		case WEST_PORTAL:
			position = new RSTile(spawnLocation.getX() - 28, spawnLocation.getY() - 17, spawnLocation.getPlane());
			break;
		case SOUTH_WEST_PORTAL:
			position = new RSTile(spawnLocation.getX() - 11, spawnLocation.getY() - 39, spawnLocation.getPlane());
		break;
		case SOUTH_EAST_PORTAL:
			position = new RSTile(spawnLocation.getX() + 13, spawnLocation.getY() - 38, spawnLocation.getPlane());
		break;
		case EAST_PORTAL:
			position = new RSTile(spawnLocation.getX() + 24, spawnLocation.getY() - 20, spawnLocation.getPlane());
		break;
		case VOID_KNIGHT:
			position = new RSTile(spawnLocation.getX() + 1, spawnLocation.getY() - 15, spawnLocation.getPlane());
		break;
		case EAST_GATE:
			position = new RSTile(spawnLocation.getX() + 15, spawnLocation.getY() - 15, spawnLocation.getPlane());
		break;
		case SOUTH_GATE:
			position = new RSTile(spawnLocation.getX() + 1, spawnLocation.getY() - 23, spawnLocation.getPlane());
		break;
		case WEST_GATE:
			position = new RSTile(spawnLocation.getX() - 13, spawnLocation.getY() - 15, spawnLocation.getPlane());
		break;
		}
	}
	
	/**
	 * Get the position of the entity.
	 * @return RStile of the position of the entity.
	 */
	public RSTile getPosition()
	{
		return position;
	}
	
	/**
	 * Sets all the positions to null.
	 */
	public static void resetPositions()
	{
		for(Entity e : Entity.values())
			e.position = null;
	}
	
	/**
	 * Get the state of a portal. Destroyed or not destroyed.
	 * @return true when the current Entity is a portal and it is destroyed. Returns false otherwise.
	 */
	public boolean isDestroyed()
	{
		int childInterfaceId = -1;
		switch(this)
		{
		case WEST_PORTAL:
			childInterfaceId = 13;
			break;
		case EAST_PORTAL:
			childInterfaceId = 14;
			break;
		case SOUTH_EAST_PORTAL:
			childInterfaceId = 15;
			break;
		case SOUTH_WEST_PORTAL:
			childInterfaceId = 16;
			break;
		}
		
		RSInterfaceChild portalInterface = Interfaces.get(408, childInterfaceId);
		if(portalInterface != null)
		{
			try
			{
				int portalHP = Integer.parseInt(portalInterface.getText());
				
				if(portalHP > 0)
					return false;
				return true;
			}
			catch (Exception e)
			{
				General.println("Failed parsing portal: " + this.toString() + " HP.");
			}
		}
		
		return true;
	}
	
	/**
	 * Get the next available portal.
	 * A portal is available when it's closest to the RSPlayer and when it's not destroyed.
	 * @return an Entity object of the next available portal.
	 */
	public static Entity getNextPortal()
	{
		int closestDistance = Integer.MAX_VALUE;
		Entity closestPortalAlive = WEST_PORTAL;
		
		for(Entity p : values())
		{
			if((p == EAST_PORTAL || p == SOUTH_EAST_PORTAL || p == SOUTH_WEST_PORTAL || p == WEST_PORTAL) && p.getPosition() != null)
			{
			
				int dist = p.getPosition().distanceTo(Player.getPosition());
				
				if(!p.isDestroyed() && dist < closestDistance)
				{
					closestDistance = dist;
					closestPortalAlive = p;
				}
			}
		}
		Variables.currentGoal = closestPortalAlive;
		return closestPortalAlive;
	}
	
	
}
