package scripts.xPestControl;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;

import javax.imageio.ImageIO;

import org.tribot.api.General;
import org.tribot.api.Timing;
import org.tribot.api2007.Combat;
import org.tribot.api2007.NPCs;
import org.tribot.api2007.Player;
import org.tribot.api2007.Skills;
import org.tribot.api2007.Skills.SKILLS;
import org.tribot.script.Script;
import org.tribot.script.ScriptManifest;
import org.tribot.script.interfaces.EventBlockingOverride;
import org.tribot.script.interfaces.Painting;
import org.tribot.script.interfaces.EventBlockingOverride.OVERRIDE_RETURN;

import scripts.xPestControl.Nodes.Antiban;
import scripts.xPestControl.Nodes.AttackMonster;
import scripts.xPestControl.Nodes.BuyItem;
import scripts.xPestControl.Nodes.EnterBoat;
import scripts.xPestControl.Nodes.RelaxInBoat;
import scripts.xPestControl.Nodes.SetGameSpawnLocation;
import scripts.xPestControl.Nodes.ToBoat;
import scripts.xPestControl.Nodes.UseSpecialAttack;
import scripts.xPestControl.Nodes.WalkToDestination;
import scripts.xPestControl.User.Constants;
import scripts.xPestControl.User.Variables;
import scripts.xPestControl.Utils.BoatType;
import scripts.xPestControl.Utils.Entity;
import scripts.xPestControl.Utils.Game;
import scripts.xPestControl.Utils.GameType;
import scripts.xPestControl.Utils.Node;

@ScriptManifest(authors = { "xCode" }, category = "Mini games", name = "xPestControl")
public class xPestControl extends Script implements Painting{
	
	private String status = "";
	private boolean debug = false;

	@Override
	public void run() {
		initialize();
		
		Variables.gui.setVisible(true);
		
		while(Variables.gui.isVisible())
		{
			General.sleep(100);
		}

		while(!Variables.stop)
		{
			for(Node n : Variables.nodes)
			{
				if(n.isValid())
				{
					status = n.getClass().getSimpleName();
					n.execute();
				}
			}
			
			General.sleep(30, 100);
		}
	}

	/**
	 * Add all nodes to the nodes list in the Variables class.
	 */
	private void initialize()
	{
		General.useAntiBanCompliance(true);
		Variables.nodes.add(new BuyItem());
		Variables.nodes.add(new ToBoat());
		Variables.nodes.add(new EnterBoat());
		Variables.nodes.add(new AttackMonster());
		Variables.nodes.add(new Antiban());
		Variables.nodes.add(new SetGameSpawnLocation());
		Variables.nodes.add(new WalkToDestination());
		Variables.nodes.add(new RelaxInBoat());
		Variables.nodes.add(new UseSpecialAttack());
	}

	@Override
	public void onPaint(Graphics gg) {
		Graphics2D g = (Graphics2D) gg;
		if(debug)
		{
			g.drawString("Status: " + status, 565, 242);
			g.drawString("Pest Points: " + Variables.totalPoints, 565, 262);
			g.drawString("West down: " + Entity.WEST_PORTAL.isDestroyed(), 565, 282);
			g.drawString("East down: " + Entity.EAST_PORTAL.isDestroyed(), 565, 302);
			g.drawString("SW down: " + Entity.SOUTH_WEST_PORTAL.isDestroyed(), 565, 322);
			g.drawString("SE down: " + Entity.SOUTH_EAST_PORTAL.isDestroyed(), 565, 342);
			g.drawString("Games won: " + Variables.gamesWon, 565, 362);
			g.drawString("Games lost: " + Variables.gamesLost, 565, 382);
			g.drawString("" + Variables.game.getGameType().getNextDestination(), 565, 402);
			g.drawString(Variables.currentGoal.toString(), 565, 422);
			g.drawString("under att: " + Combat.isUnderAttack(),200, 200);
			g.drawString("attacking entities: " + Combat.getAttackingEntities().length,200, 220);
			g.drawString("In combat: " + Player.getRSPlayer().isInCombat(),200, 240);
		}
		
		String runTime = Timing.msToString((Timing.currentTimeMillis() - Constants.START_TIME));
		int gainedPoints = Variables.gamesWon * Variables.game.getBoatType().getPoints();
		double pointsPH = ((double)gainedPoints * (3600000.0 / (Timing.currentTimeMillis() -  Constants.START_TIME)));
		String nextItem = "None";
		int pointsLeft = 0;
		
		if(Variables.itemsToBuy.size() > 0)
		{
			nextItem = Variables.itemsToBuy.get(0).toString();
			pointsLeft = Variables.itemsToBuy.get(0).getCost() - Variables.totalPoints;
		}
		
		int paintX = 30;
		int paintY = 283;
		int row2offsetX = 290;
		
		g.setColor(Constants.TRANSP_BLACK_COLOR);
		g.fillRect(3, 246, 513, 93);
		
		g.setColor(Color.white);
		
		g.setFont(Constants.TITLE_FONT);
		g.drawString("xPest Control", 195, 265);
		
		g.setFont(Constants.TEXT_ITALIC_FONT);
		g.drawString("by xCode", 215, 280);
		
		g.setFont(Constants.TEXT_FONT);
		
		g.drawString("Runtime: " + runTime, paintX, paintY - 15);
		g.drawString("Points gained: " + gainedPoints + " [" + (int)pointsPH + " P/H]", paintX, paintY);
		g.drawString("Games: Won [" + Variables.gamesWon + "] Lost [" + Variables.gamesLost + "]", paintX, paintY + 15);
		g.drawString("Next item: " + nextItem + " [" + pointsLeft + " points left]", paintX, paintY + 30);
		
		g.drawString("Boat: " + Variables.game.getBoatType().toString(), paintX + row2offsetX, paintY - 15);
		g.drawString("Type: " + Variables.game.getGameType().toString(), paintX + row2offsetX, paintY);
		g.drawString("Status: " + status, paintX + row2offsetX, paintY + 15);
		
		if(Variables.itemsToBuy.size() > 0)
		{
			int percentageToItem = (int)(100.0 / Variables.itemsToBuy.get(0).getCost() * Variables.totalPoints);
			g.setColor(Constants.TRANSP_GREEN_COLOR);
			int greenBarWidth = (int) (513.0 * (percentageToItem / 100.0));
			g.fillRect(4, 322, greenBarWidth, 16);
			g.setColor(Color.DARK_GRAY);
			g.drawLine(4, 322, 517, 322);
			g.setColor(Color.WHITE);
			g.drawString(percentageToItem + "% until we can buy a " +  Variables.itemsToBuy.get(0).toString(), 155, 335);
		}
	}
}
