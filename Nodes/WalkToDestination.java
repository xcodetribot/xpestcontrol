package scripts.xPestControl.Nodes;

import org.tribot.api.General;
import org.tribot.api.Timing;
import org.tribot.api.types.generic.Condition;
import org.tribot.api.types.generic.Filter;
import org.tribot.api2007.Combat;
import org.tribot.api2007.NPCs;
import org.tribot.api2007.Objects;
import org.tribot.api2007.PathFinding;
import org.tribot.api2007.Player;
import org.tribot.api2007.Walking;
import org.tribot.api2007.WebWalking;
import org.tribot.api2007.types.RSNPC;
import org.tribot.api2007.types.RSObject;
import org.tribot.api2007.types.RSObjectDefinition;
import org.tribot.api2007.util.DPathNavigator;

import scripts.xPestControl.User.Constants;
import scripts.xPestControl.User.Variables;
import scripts.xPestControl.Utils.Node;

public class WalkToDestination extends Node{

	final DPathNavigator dpn = new DPathNavigator();
	final Filter<RSObject> doorFilter = new Filter<RSObject>() {

		@Override
		public boolean accept(RSObject door) {
			RSObjectDefinition definition = door.getDefinition();
			if(definition != null)
			{
				if(!definition.getName().equals("Gate"))
					return false;
				String[] actions = definition.getActions();
				if(actions.length > 1)
				{
					if(actions[1].equals("Repair"))
						return false;
				}
			}
			return true;
		}};
	
	@Override
	public boolean isValid() {
		RSNPC[] monsters = NPCs.findNearest(Constants.getMonsters());
		return Variables.game.getGameType().getNextDestination() != null && !Variables.game.getGameType().isAtDestination() && Variables.game.inGame() && !Player.getRSPlayer().isInCombat() && Variables.game.getBoatType().getTile().distanceTo(Variables.game.getGameType().getNextDestination()) > 40 && ((monsters.length > 0 && monsters[0].getPosition().distanceTo(Player.getPosition()) > 2) || monsters.length == 0);
	}

	@Override
	public void execute() {
		final long waitTime = General.random(2000, 3000);
		final long currentTime = Timing.currentTimeMillis();
		dpn.setStoppingCondition(new Condition() {
			
			@Override
			public boolean active() {
				final RSNPC[] monsters = NPCs.findNearest(Constants.getMonsters());
				return Variables.game.getGameType().isAtDestination() || !Variables.game.inGame() || (Timing.currentTimeMillis() - currentTime >= waitTime) || (monsters.length > 0 && monsters[0].getPosition().distanceTo(Player.getPosition()) <= 4);
			}
		});
		dpn.setStoppingConditionCheckDelay(General.random(500, 700));
		dpn.overrideDoorCache(true, Objects.findNearest(40, doorFilter));
		dpn.traverse(Variables.game.getGameType().getNextDestination());
	}

}
