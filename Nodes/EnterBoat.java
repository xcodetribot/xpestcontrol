package scripts.xPestControl.Nodes;

import org.tribot.api.DynamicClicking;
import org.tribot.api.General;
import org.tribot.api.Timing;
import org.tribot.api.types.generic.Condition;
import org.tribot.api.types.generic.Filter;
import org.tribot.api2007.Interfaces;
import org.tribot.api2007.NPCChat;
import org.tribot.api2007.NPCs;
import org.tribot.api2007.Objects;
import org.tribot.api2007.Player;
import org.tribot.api2007.Walking;
import org.tribot.api2007.types.RSInterfaceChild;
import org.tribot.api2007.types.RSNPC;
import org.tribot.api2007.types.RSObject;

import scripts.xPestControl.xPestControl;
import scripts.xPestControl.User.Constants;
import scripts.xPestControl.User.Variables;
import scripts.xPestControl.Utils.Entity;
import scripts.xPestControl.Utils.Node;

public class EnterBoat extends Node{

	@Override
	public boolean isValid() {
		final RSObject[] gangplanks = Objects.findNearest(5, "Gangplank");
		final RSNPC[] squires = NPCs.find(new Filter<RSNPC>(){

			@Override
			public boolean accept(RSNPC npc) {
				String name = npc.getName();
				return name != null && name.contains("Squire");
			}
			
		});
		
		RSInterfaceChild pestPointInterface = Interfaces.get(Constants.BOAT_INTERFACE, 14);
		if(pestPointInterface != null)
		{
			String pestPoints = pestPointInterface.getText().substring(13);
			try
			{
				Variables.totalPoints = Integer.parseInt(pestPoints);
			}
			catch (Exception e)
			{
				General.println("Can't parse Pest Points to int.");
			}
		}
		
		if(Variables.totalPoints == 250 && Variables.itemsToBuy.size() == 0)
		{
			General.println("We have 250 Pest Points, but don't know how to spend it :( Stopping script.");
			Variables.stop = true;
		}
		
		return squires.length > 0 && gangplanks.length > 0 && !Variables.game.getBoatType().getBoatArea().contains(Player.getPosition()) && Interfaces.get(Constants.MINIGAME_INTERFACE) == null && (Variables.itemsToBuy.size() == 0 || Variables.totalPoints  < Variables.itemsToBuy.get(0).getCost());
	}

	@Override
	public void execute() {
		String npcMessage = NPCChat.getMessage();
		
		final RSObject[] gangplanks = Objects.findNearest(5, "Gangplank");
		if(gangplanks.length > 0)
		{
			if(!gangplanks[0].isOnScreen())
			{
				Walking.walkTo(gangplanks[0].getPosition());
			}
			if(DynamicClicking.clickRSObject(gangplanks[0], "Cross"))
			{
				if(npcMessage != null && npcMessage.contains("Congratulations"))
					Variables.gamesWon++;
				else if(npcMessage != null && (npcMessage.contains("lack") || npcMessage.contains("killed")))
					Variables.gamesLost++;
				
				Entity.resetPositions();
				Timing.waitCondition(new Condition() {
					
					@Override
					public boolean active() {
						return Variables.game.inBoatArea();
					}
				}, General.random(400, 600));
			}
		}
	}

}
