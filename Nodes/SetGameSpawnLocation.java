package scripts.xPestControl.Nodes;

import org.tribot.api.General;
import org.tribot.api.Timing;
import org.tribot.api.types.generic.Condition;
import org.tribot.api2007.NPCChat;
import org.tribot.api2007.NPCs;
import org.tribot.api2007.Player;
import org.tribot.api2007.types.RSNPC;
import org.tribot.api2007.types.RSTile;

import scripts.xPestControl.User.Variables;
import scripts.xPestControl.Utils.Entity;
import scripts.xPestControl.Utils.Node;

public class SetGameSpawnLocation extends Node{

	@Override
	public boolean isValid() {
		return Variables.game.inGame() && NPCs.findNearest("Squire").length > 0 && !Variables.game.getBoatType().getBoatArea().contains(Player.getPosition()) && (Variables.game.getGameType().getNextDestination() == null || Variables.game.getGameType().getNextDestination().distanceTo(Variables.game.getBoatType().getTile()) < 40);
	}

	@Override
	public void execute() {
		Timing.waitCondition(new Condition() {
			
			@Override
			public boolean active() {
				return NPCs.findNearest("Squire").length == 1;
			}
		}, General.random(1000, 1200));
		RSNPC[] squire = NPCs.findNearest("Squire");
		if(squire.length > 0)
		{
			RSTile squirePos = squire[0].getPosition();
			for(Entity e : Entity.values())
				e.setPosition(squirePos);
		}
	}

}
