package scripts.xPestControl.Nodes;

import org.tribot.api.General;
import org.tribot.api.types.generic.Condition;
import org.tribot.api.types.generic.Filter;
import org.tribot.api2007.Interfaces;
import org.tribot.api2007.NPCs;
import org.tribot.api2007.Objects;
import org.tribot.api2007.Player;
import org.tribot.api2007.WebWalking;
import org.tribot.api2007.types.RSInterfaceChild;
import org.tribot.api2007.types.RSNPC;
import org.tribot.api2007.types.RSObject;

import scripts.xPestControl.User.Constants;
import scripts.xPestControl.User.Variables;
import scripts.xPestControl.Utils.Node;

public class ToBoat extends Node{

	@Override
	public boolean isValid() {
		final RSObject[] gangplanks = Objects.findNearest(5, "Gangplank");
		final RSNPC[] squires = NPCs.find(new Filter<RSNPC>(){

			@Override
			public boolean accept(RSNPC npc) {
				String name = npc.getName();
				return name != null && name.contains("Squire");
			}
		});

		return (squires.length == 0 || gangplanks.length == 0 || (squires.length > 0 && !squires[0].isOnScreen() || gangplanks.length > 0 && !gangplanks[0].isOnScreen())) && !Variables.game.getBoatType().getBoatArea().contains(Player.getPosition()) && Interfaces.get(Constants.MINIGAME_INTERFACE) == null && (Variables.itemsToBuy.size() == 0 || Variables.totalPoints  < Variables.itemsToBuy.get(0).getCost());
	}

	@Override
	public void execute() {
		WebWalking.walkTo(Variables.game.getBoatType().getTile(), new Condition() {
			
			@Override
			public boolean active() {
				final RSObject[] gangplanks = Objects.findNearest(5, "Gangplank");
				final RSNPC[] squires = NPCs.find(new Filter<RSNPC>(){

					@Override
					public boolean accept(RSNPC npc) {
						String name = npc.getName();
						return name != null && name.contains("Squire");
					}
					
				});
				
				return gangplanks.length > 0 && squires.length > 0 && gangplanks[0].isOnScreen();
			}
		}, General.random(3000, 4000));
	}

}
