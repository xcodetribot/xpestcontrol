package scripts.xPestControl.Nodes;

import org.tribot.api.General;
import org.tribot.api.Timing;
import org.tribot.api2007.Walking;

import scripts.xPestControl.User.Constants;
import scripts.xPestControl.User.Variables;
import scripts.xPestControl.Utils.Game;
import scripts.xPestControl.Utils.Node;

public class RelaxInBoat extends Node{
	
	long nextWalk = Timing.currentTimeMillis();

	@Override
	public boolean isValid() {
		return Variables.game.inBoatArea() && Timing.currentTimeMillis() >= nextWalk;
	}

	@Override
	public void execute() {
		Walking.walkTo(Variables.game.getBoatType().getBoatArea().getRandomTile());
		nextWalk = Timing.currentTimeMillis() + General.random(20000, 55000);
	}

}
