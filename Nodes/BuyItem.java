package scripts.xPestControl.Nodes;

import java.awt.Point;
import java.awt.Rectangle;

import org.tribot.api.DynamicClicking;
import org.tribot.api.General;
import org.tribot.api.Timing;
import org.tribot.api.input.DynamicMouse;
import org.tribot.api.input.Mouse;
import org.tribot.api.types.generic.Condition;
import org.tribot.api.types.generic.CustomRet_0P;
import org.tribot.api2007.Interfaces;
import org.tribot.api2007.NPCs;
import org.tribot.api2007.Player;
import org.tribot.api2007.WebWalking;
import org.tribot.api2007.types.RSInterfaceChild;
import org.tribot.api2007.types.RSNPC;

import scripts.xPestControl.User.Constants;
import scripts.xPestControl.User.Variables;
import scripts.xPestControl.Utils.Item;
import scripts.xPestControl.Utils.Node;

public class BuyItem extends Node{

	@Override
	public boolean isValid() {
		return Variables.itemsToBuy.size() > 0 && Variables.totalPoints >= Variables.itemsToBuy.get(0).getCost() && !Variables.game.getBoatType().getBoatArea().contains(Player.getPosition()) && Interfaces.get(Constants.MINIGAME_INTERFACE) == null;
	}

	@Override
	public void execute() {
		final RSNPC[] voidKnights = NPCs.findNearest("Void Knight");
		
		if(voidKnights.length > 0 && Interfaces.get(Constants.SHOP_INTERFACE) == null)
		{
			if(!voidKnights[0].isOnScreen())
			{
				WebWalking.walkTo(voidKnights[0], new Condition(){

					@Override
					public boolean active() {
						return voidKnights[0].isOnScreen();
					}
				}, General.random(3000, 4000));
			}
			if(DynamicClicking.clickRSNPC(voidKnights[0], "Exchange"))
			{
				Timing.waitCondition(new Condition() {
					
					@Override
					public boolean active() {
						return Interfaces.get(Constants.SHOP_INTERFACE) != null;
					}
				}, General.random(1500, 2000));
			}
		}
		
		if(Interfaces.get(Constants.SHOP_INTERFACE) != null)
		{
			final Item item = Variables.itemsToBuy.get(0);
			final RSInterfaceChild itemToBuy = Interfaces.get(Constants.SHOP_INTERFACE, item.getChildInterface());
			if(itemToBuy != null && itemToBuy.getAbsolutePosition().getY() > 249)
			{
				if(DynamicMouse.click(new CustomRet_0P<Point>() {

					@Override
					public Point ret() {
						return new Point((int)item.getScrollBarRect().getX(), (int) item.getScrollBarRect().getY());
					}
				}, 1))
				{
					Timing.waitCondition(new Condition() {
						
						@Override
						public boolean active() {
							return itemToBuy.getAbsolutePosition().getY() <= 249;
						}
					}, General.random(1000, 1200));
				}
				
			}
			if(itemToBuy != null && itemToBuy.getAbsolutePosition().getY() <= 249)
			{
				if(itemToBuy.click("Choose"))
				{
					Timing.waitCondition(new Condition() {
						
						@Override
						public boolean active() {
							RSInterfaceChild child = Interfaces.get(Constants.SHOP_INTERFACE, Constants.EXCHANGE_CONFIRM_ITEM_NAME_CHILD);
							return child != null && child.getText() == item.toString();
						}
					}, General.random(800, 1200));
				}
			}
			RSInterfaceChild confirmChild = Interfaces.get(Constants.SHOP_INTERFACE, Constants.EXCHANGE_CONFIRM_ITEM_NAME_CHILD);
			if(confirmChild != null && confirmChild.getText() != "")
			{
				RSInterfaceChild confirmBox = Interfaces.get(Constants.SHOP_INTERFACE, Constants.EXCHANGE_CONFIRM_BOX_CHILD);
				if(confirmBox != null)
				{
					if(confirmBox.click("Confirm"))
					{
						Variables.totalPoints -= Variables.itemsToBuy.get(0).getCost();
						Variables.itemsToBuy.remove(0);
					}
				}
			}
		}
		
	}

}
