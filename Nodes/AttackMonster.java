package scripts.xPestControl.Nodes;

import org.tribot.api.DynamicClicking;
import org.tribot.api.General;
import org.tribot.api.Timing;
import org.tribot.api.types.generic.Condition;
import org.tribot.api.types.generic.Filter;
import org.tribot.api2007.Combat;
import org.tribot.api2007.Game;
import org.tribot.api2007.Interfaces;
import org.tribot.api2007.NPCs;
import org.tribot.api2007.Objects;
import org.tribot.api2007.Options;
import org.tribot.api2007.Player;
import org.tribot.api2007.Prayer;
import org.tribot.api2007.Skills;
import org.tribot.api2007.Skills.SKILLS;
import org.tribot.api2007.Walking;
import org.tribot.api2007.WebWalking;
import org.tribot.api2007.types.RSCharacter;
import org.tribot.api2007.types.RSNPC;
import org.tribot.api2007.types.RSObject;
import org.tribot.api2007.types.RSObjectDefinition;
import org.tribot.api2007.util.DPathNavigator;

import scripts.xPestControl.User.Constants;
import scripts.xPestControl.User.Variables;
import scripts.xPestControl.Utils.Node;

public class AttackMonster extends Node{

	final Filter<RSNPC> monsterFilter = new Filter<RSNPC>() {

		@Override
		public boolean accept(RSNPC npc) {
			boolean acceptMonster = false;
			for(String s : Constants.getMonsters())
			{
				if(npc.getName().equals(s))
				{
					acceptMonster = true;
					break;
				}
			}
			return acceptMonster && npc.getHealth() > 0;
		}};
		final Filter<RSObject> doorFilter = new Filter<RSObject>() {

			@Override
			public boolean accept(RSObject door) {
				RSObjectDefinition definition = door.getDefinition();
				if(definition != null)
				{
					if(!definition.getName().equals("Gate"))
						return false;
					String[] actions = definition.getActions();
					if(actions.length > 1)
					{
						if(actions[1].equals("Repair"))
							return false;
					}
				}
				return true;
			}};
		DPathNavigator dpn = new DPathNavigator();
		
	@Override
	public boolean isValid() {
		final RSCharacter targetEntity = Combat.getTargetEntity();
		return Interfaces.get(Constants.MINIGAME_INTERFACE) != null && (targetEntity == null || (targetEntity != null && !targetEntity.isInCombat()));
	}

	@Override
	public void execute() {
		if(!isQuickPrayerEnabled() && Variables.useQuickPray && Skills.getCurrentLevel(SKILLS.PRAYER) > 0)
			Options.setQuickPrayersOn(true);
		
		final RSNPC[] monsters = NPCs.findNearest(Constants.getMonsters());
		NPCs.findNearest(monsterFilter);
		if(monsters.length > 0)
		{
			if(!monsters[0].isOnScreen())
			{
				dpn.setStoppingCondition(new Condition() {
					
					@Override
					public boolean active() {
						return monsters[0].isOnScreen() || !Variables.game.inGame();
					}});
				dpn.setStoppingConditionCheckDelay(General.random(800, 1400));
				dpn.overrideDoorCache(true, Objects.findNearest(40, doorFilter));
				dpn.traverse(monsters[0].getPosition());
			}
			else
			{
				if(DynamicClicking.clickRSNPC(monsters[0], "Attack"))
				{
					final RSCharacter targetEntity = Combat.getTargetEntity();
					Variables.currentMonster = monsters[0];
					Timing.waitCondition(new Condition() {
						
						@Override
						public boolean active() {
							return (targetEntity != null && targetEntity.isInCombat()) || !Variables.game.inGame();
						}
					}, General.random(2000, 3000));
				}
			}
		}
		
	}
	
	/**
	 * Is quick prayer currently on?
	 * @return true when it's on, else otherwise.
	 */
	private boolean isQuickPrayerEnabled(){
		if (Game.getSettingsArray()[375] == 9)
		   return true;
		return false;
	}

}
