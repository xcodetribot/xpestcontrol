package scripts.xPestControl.Nodes;

import org.tribot.api2007.Combat;
import org.tribot.api2007.Game;
import org.tribot.api2007.Options;
import org.tribot.api2007.Player;
import org.tribot.api2007.Skills.SKILLS;

import scripts.xPestControl.User.Constants;
import scripts.xPestControl.Utils.Node;

public class Antiban extends Node{

	@Override
	public boolean isValid() {
		return !Player.getRSPlayer().isInCombat();
	}

	@Override
	public void execute() {
		Constants.ANTIBAN.performCombatCheck();

		Constants.ANTIBAN.performEquipmentCheck();

		Constants.ANTIBAN.performExamineObject();

		Constants.ANTIBAN.performFriendsCheck();

		Constants.ANTIBAN.performLeaveGame();

		Constants.ANTIBAN.performMusicCheck();

		Constants.ANTIBAN.performPickupMouse();

		Constants.ANTIBAN.performQuestsCheck();

		Constants.ANTIBAN.performRandomMouseMovement();

		Constants.ANTIBAN.performRandomRightClick();

		Constants.ANTIBAN.performRotateCamera();

		SKILLS combatType;
		switch(Combat.getSelectedStyleIndex())
		{
		case 0:
			combatType = SKILLS.ATTACK;
			break;
		case 1:
			combatType = SKILLS.STRENGTH;
			break;
		case 3:
			combatType = SKILLS.DEFENCE;
			break;
		default:
			combatType = SKILLS.STRENGTH;
			break;
		}
		
		Constants.ANTIBAN.performTimedActions(combatType);

		Constants.ANTIBAN.performXPCheck(combatType);
		
		if(Player.isMoving() && Game.getRunEnergy() >= Constants.ANTIBAN.INT_TRACKER.NEXT_RUN_AT.next())
		{
			Options.setRunOn(true);
			Constants.ANTIBAN.INT_TRACKER.NEXT_RUN_AT.reset();
		}
		
	}

}
